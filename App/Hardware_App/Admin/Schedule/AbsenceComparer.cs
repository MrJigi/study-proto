﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hardware_App
{
    public class AbsenceComparer : IComparer<EmployeeAbsence>
    {
        public int Compare(EmployeeAbsence x, EmployeeAbsence y)
        {
            int cmp = 0;

            cmp = x.TicketStatus.Length.CompareTo(y.TicketStatus.Length);

            if (cmp == 0)
            {
                cmp = x.TicketStatus.CompareTo(y.TicketStatus);

            }

            if (cmp == 0)
            {
                cmp = x.Id.CompareTo(y.Id);
            }

            return cmp;
        }

        public AbsenceComparer()
        {
        }
    }
}
